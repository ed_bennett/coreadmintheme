<?php

Class MyCore extends Helper {

    /**
     * Variable to load widgets
     *
     * @package MyCore
     * @since 0.1.2 Variable introduced
     * @var null
     *
     */
    public static $widget;

    /**
     * Variable to load styles
     *
     * @package MyCore
     * @since 0.1.2 Variable introduced
     * @var null
     *
     */
    public static $styles;

    /**
     * __construct function for class
     *
     * @package MyCore
     * @since 0.1.0 Function first introduced
     * @since 0.1.1 Pages now created on class call
     * @return null Runs Automatically when class instantiated
     *
     */
    public function __construct() {
        // No output here

        // Remove Welcome Panel
        remove_action( 'welcome_panel', 'wp_welcome_panel' );

        // Disable Admin bar on backend  and frontend
        add_action( 'admin_init', array( $this, 'remove_admin_bar' ) );

        // Create admin pages
        add_action('admin_menu', array( $this, 'create_pages' ) );;

        // Change footer text
        add_action( 'admin_init', array( $this, 'change_footer_text'), 11 );

        // Force 1 column layout
        add_filter( 'screen_layout_columns', function($col) {
            $columns['dashboard'] = 1;
            return $columns;
        });

        // Force 1 column layout
        add_filter( 'get_user_option_screen_layout_dashboard', function() {
            return 1;
        });

        // Remove Screen Options Tab
        add_filter('screen_options_show_screen', '__return_false');

        // Unset default dashboard widgets
        add_action('wp_dashboard_setup', function() {
            global $wp_meta_boxes;
            remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
            remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
            remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
            remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
        });

        add_action( 'current_screen', array( $this, 'dashboard_redirect' ) );

    }

    /**
     * Activation code
     *
     * @package MyCore
     * @since 0.1.0 Function first introduced
     * @deprecated since v0.1.2
     * @return Amends Footer text. Not to be called directly.
     *
     */
    public function core_activate() {
        trigger_error( "This function is now deprecated. Everything in here now runs from the __construct() function.", E_USER_NOTICE );
        return;

    }

    /**
     * Page creation
     *
     * @package MyCore
     * @since 0.1.0 Function first introduced
     * @since 0.1.1 Deprecated
     * @deprecated since v0.1.1
     * @return null Generates a menu page for Wordpress.
     *
     */
    public function core_page_creation() {
        trigger_error("This function is deprecated. Please use the built in helper class.", E_USER_NOTICE);
        return;
    }

    /**
     * Callback function
     *
     * @package MyCore
     * @since 0.1.0 Function first created
     * @since 0.1.1 Function renamed
     * @return file This is the callback function for page creation
     *
     */
    public function parent_content() {
        
    }

    /**
     * Callback function
     *
     * @package MyCore
     * @since 0.1.0 Function first created
     * @return null Removes admin bar in Backend and Frontend
     *
     */
    public function remove_admin_bar() {

        wp_deregister_script( 'admin-bar' );
        wp_deregister_style( 'admin-bar' );

        remove_action( 'admin_init', '_wp_admin_bar_init' );
        remove_action( 'in_admin_header', 'wp_admin_bar_render', 0 );

    }

    public function create_pages() {
        $pages = array(
            array(
                'page_title'            =>      'Custom Dashboard',
                'menu_title'            =>      'Custom Dashboard',
                'page_slug'             =>      'custom-dashboard',
                'capability'            =>      'update_core',
                'callback_function'     =>      array( $this, 'custom_dashboard_content'),
                'icon'                  =>      ''
            )
        );

        foreach ( $pages as $page ) :

            add_menu_page(
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['page_slug'],
                $page['callback_function'],
                $page['icon']
            );

        endforeach;
    }

    public function custom_dashboard_content() {
        if( is_admin() ) require_once( dirname( dirname( __FILE__ ) ) . '/views/index.php');
        return;
    }
    
    public function change_footer_text() {
        add_filter( 'admin_footer_text', function() {
            return 'Made by Ed';
        });
    
        add_filter( 'update_footer', function() {
            return 'Core Plugin Version: ' . COREPLUGINVER . '. Wordpress version: '. get_bloginfo('version');
        });
    }

    public function dashboard_redirect() {
        // Perform a redirect to the new custom dashboard
        if ( is_admin() ) :

            $page = get_current_screen();

            if ( $page->base == 'dashboard' ) wp_redirect( admin_url( 'index.php?page=custom-dashboard' ) );

        endif;
    }

}
