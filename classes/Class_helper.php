<?php

Class Helper {

    /**
     * Constructor
     *
     * @package MyCore
     * @since 0.1.1 Function first introduced
     * @return null Runs when class is run
     *
     */
     
    public function __construct() {
        // No output here
    }
}