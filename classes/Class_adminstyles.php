<?php

Class AdminStyles {
    public function init() {
        add_action( 'admin_enqueue_scripts', function() {
            wp_register_style( 'mycore_custom_style', plugin_dir_url( dirname(__FILE__) ) . 'styles/css/mycore_custom.css', false, '0.1.0' );
            wp_enqueue_style( 'mycore_custom_style' );    
        }, 12);        
    }
}
