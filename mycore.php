<?php

/***********************************************************
Plugin Name: Ed's WP Core
Plugin URI: http://edswebdev.space
Description: My core plugin
Version: 0.1.2
Author: Ed Bennett
Author URI: http://edthewebdev.pro
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
***********************************************************/

add_action( 'admin_init', function() {
    $ver =  get_plugin_data( __FILE__, false, false );
    define('COREPLUGINVER', $ver['Version']);
});

// Auto load all the classes required
spl_autoload_register( function( $className ) {
    $lower = strtolower( $className );
    $dir = dirname(__FILE__) . '/classes/Class_';
    $class = $dir . $lower . '.php';
    if( file_exists( $class ) ) require $class;
});

new MyCore;
MyCore::$widget = new Widgets;

$adminStyles        =   new AdminStyles;

$adminStyles->init();
